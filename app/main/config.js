angular.module('main')
.config(function(multiselectProvider) {
    multiselectProvider.setTemplateUrl('lib/ionic-multiselect/dist/templates/item-template.html');
    multiselectProvider.setModalTemplateUrl('lib/ionic-multiselect/dist/templates/modal-template.html');
})
.config(['RestangularProvider',function(RestangularProvider) {
  RestangularProvider.setBaseUrl('http://apipasefit.arawato.com.ve/api/');
}])
.config(function($ionicConfigProvider) {
  $ionicConfigProvider.scrolling.jsScrolling(false);
  if( ionic.Platform.isAndroid() ) {
    $ionicConfigProvider.scrolling.jsScrolling(false);
  }
})
// CONFIGURACION DE DATE PICKER
.config(function (ionicDatePickerProvider) {
    var datePickerObj = {
      inputDate: new Date(),
      mondayFirst: true,
      weeksList: ["DO", "LU", "MA", "MI", "JU", "VI", "SA"],
      monthsList: ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DIC"],
      templateType: 'popup',
      from: new Date(2016, 12, 12),
      to: new Date(2020, 8, 1),
      dateFormat: 'dd MMMM yyyy',
      closeOnSelect: true,
      titleLabel: 'Selecciona una fecha',
      setLabel: 'Selec',
      todayLabel: 'Hoy',
      closeLabel: 'Salir'
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);
})
// CONFIGURACION DE TIME PICKER
.config(function (ionicTimePickerProvider) {
    var timePickerObj = {
      format: 12,
      step: 10,
      setLabel: 'SELECT',
      closeLabel: 'CERRAR'
    };
    ionicTimePickerProvider.configTimePicker(timePickerObj);
})
.config(
  function Config($httpProvider, jwtOptionsProvider) {
  jwtOptionsProvider.config({
    whiteListedDomains: ['apipasefit.arawato.com.ve','localhost'],
    tokenGetter: ['$localStorage', function($localStorage) {
      return $localStorage.token;
    }]
  });
  $httpProvider.interceptors.push('jwtInterceptor');
})
.config(function($ionicCloudProvider) {
  $ionicCloudProvider.init({
    "core": {
      "app_id": "ed1ca246"
    },
    "push": {
      "sender_id": "323703019885",
      "pluginConfig": {
        "ios": {
          "badge": true,
          "sound": true
        },
        "android": {
          "iconColor": "#343434",
          "badge": true,
          "sound": true
        }
      }
    }
  });
})
.run(
  function(
    $ionicPlatform,
    $rootScope,
    session,
    $state,
    $localStorage,
    $ionicPush,
    $ionicHistory,
    $ionicLoading,
    $timeout
    ){;
  $rootScope.irAtras = function(){
      var backView = $ionicHistory.backView();
      backView.go();
  };
  function obtenerHoraActual(){
    session.obtenerHoraServidor().get('').then(
      function(r){
        $localStorage.fechaOriginal = r.date;
        var fecha = r.date.split(' ')[0];
        fecha = fecha.split('-');
        $localStorage.fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
      },
      function(r){
        // console.log(r)
      });
  }
  // BUSCAR HORA SI ESTA LOGEADO
  if($localStorage.login){
      obtenerHoraActual();
        if($localStorage.imageUrl !== null && $localStorage.imageUrl !== undefined){
          $rootScope.imageUrl = $localStorage.imageUrl;
        }
        if($localStorage.splash !== null && $localStorage.splash !== undefined){
          $rootScope.splash = $localStorage.splash;
        }
  }

  $ionicPlatform.ready(function(){
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard){
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar){
      StatusBar.styleDefault();
    }
  });

  if($localStorage.dataUsuario !== null && $localStorage.dataUsuario !== undefined)
//    if($localStorage.dataUsuario.dataStudio !== null && $localStorage.dataUsuario.dataStudio.image !== undefined){
    if($localStorage.dataUsuario.dataStudio !== null && $localStorage.dataUsuario.dataStudio !== undefined){
        $rootScope.splash = $localStorage.splash;
        $rootScope.firstName = ($localStorage.dataUsuario.dataStudio.studioName === null && $localStorage.dataUsuario.dataStudio.studioName === undefined) ? 'Cargando...' : $localStorage.dataUsuario.dataStudio.studioName;
        $rootScope.direccion = $localStorage.dataUsuario.dataStudio.studioCityName + " " + $localStorage.dataUsuario.dataStudio.studioStateName;
    }
  $rootScope.$on("$stateChangeSuccess", function(evt, to, toP, from, fromP){
      if ( (to.name === 'login' || to.name === 'carrusel' ||
              to.name === 'recuPass' || to.name === 'registrar')
              && ($localStorage.login)){
            if($localStorage.dataUsuario !== null){
               obtenerHoraActual();
               if($localStorage.dataUsuario.data.roles[0] === "ROLE_STUDIO"){
                    if($localStorage.splash !== null && $localStorage.splash !== undefined){
                      $rootScope.splash = $localStorage.dataUsuario.dataStudio.image;
                    }
                    if($localStorage.logo !== null && $localStorage.logo !== undefined){
                      $rootScope.imageUrl = $localStorage.dataUsuario.dataStudio.logo;
                    }
                    session.dataStudio($localStorage.dataUsuario.data).then(
                      function(r){
                        $localStorage.dataUsuario.dataStudio = JSON.parse(r);
                        $rootScope.firstName = ($localStorage.dataUsuario.dataStudio.studioName === null && $localStorage.dataUsuario.dataStudio.studioName === undefined) ? 'Cargando...' : $localStorage.dataUsuario.dataStudio.studioName;
                        $rootScope.imageUrl = ($localStorage.dataUsuario.dataStudio.logo === null && $localStorage.dataUsuario.dataStudio.logo === undefined ) ? 'img/default-user-image.png' :'http://api.pase.fit/uploads/images/logos/'+$localStorage.dataUsuario.dataStudio.logo;
                        $rootScope.splash = ($localStorage.dataUsuario.dataStudio.image === null && $localStorage.dataUsuario.dataStudio.image === undefined) ? 'img/converUser.png' : 'http://api.pase.fit/uploads/images/studios/'+$localStorage.dataUsuario.dataStudio.image;
                        $rootScope.direccion = $localStorage.dataUsuario.dataStudio.studioCityName+" "+$localStorage.dataUsuario.dataStudio.studioStateName;
                        $localStorage.splash = $rootScope.splash;
                        $localStorage.logo = $rootScope.imageUrl;
                        $state.go('listaStudio');
                      },
                      function(r){
                        $ionicLoading.show('Problemas al iniciar sesión...');
                        $timeout(function(){
                          $localStorage.login = false;
                          $ionicLoading.hide();
                          $state.go('login');
                        },4000);
                      });
               }
               else if($localStorage.dataUsuario.data.roles[0] === "ROLE_CLIENT"){  
                  if($localStorage.imageUrl !== null && $localStorage.imageUrl !== undefined){
                    $rootScope.imageUrl = $localStorage.imageUrl;
                  }
                  session.dataClient($localStorage.dataUsuario.data)
                  .then(
                    function(r){
                      $localStorage.dataUsuario.dataCliente = JSON.parse(r);
                      $rootScope.firstName = ($localStorage.dataUsuario.dataCliente.firstName === null && $localStorage.dataUsuario.dataCliente.firstName === undefined) ? 'Cargando...' : $localStorage.dataUsuario.dataCliente.firstName;
                      $rootScope.imageUrl = ($localStorage.dataUsuario.dataCliente.image === null && $localStorage.dataUsuario.dataCliente.image === undefined ) ? 'img/default-user-image.png' :'http://api.pase.fit/uploads/images/clients/'+$localStorage.dataUsuario.dataCliente.image;
                      $rootScope.classReserva = $localStorage.classReserva;
                      $rootScope.ciudad = $localStorage.ciudad;
                      $localStorage.imageUrl = $rootScope.imageUrl;
                      $state.go('menu.lista');
                    },
                    function(r){
                      $ionicLoading.show('Problemas al iniciar sesión...');
                      $timeout(function(){
                        $ionicLoading.hide();
                        $localStorage.login = false;
                        $state.go('login');
                      },4000);
                    });
               }
            }
            else{
              $localStorage.login = false;
            }
      }
      if( (from.name ==="login") && (!$localStorage.login) ){
          if( (to.name !== "registrar" && to.name !== "carrusel" && to.name !== 'recuPass' && to.name !== 'registrar' ) ){
            $state.go('login');
          }
      }     
  });

  $rootScope.logout = function(){
    session.cerrarSession();
    $ionicPush.unregister();
  };
})
// ADROID PUSH NOTIFICATION
.run(
  function(
    $rootScope,
    session,
    $ionicModal,
    jwtHelper,
    $localStorage,
    $timeout,
    $ionicLoading,
    $localStorage,
    $state
  ){

  if($localStorage.token !== null && $localStorage.token !== undefined){
    if(jwtHelper.isTokenExpired($localStorage.token)){
        session.refreshToken()
        .customGET(null,{refresh_token:$localStorage.dataUsuario.refresh_token})
        .then(
          function(r){
            $localStorage.dataUsuario.refresh_token = r.refresh_token;
            $localStorage.dataUsuario.token = r.token;
            $localStorage.token = r.token;
          },
          function(r){
            $ionicLoading.show({template:"Problemas con comunicación con el servidor!"});
            $timeout(function(){
                $ionicLoading.hide();
                $localStorage.$reset();
                $state.go('login');
                $localStorage.login = false;
            },3000);
          }
        );
    }
  }

  $rootScope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
  };

  $rootScope.$on('cloud:push:notification', function(event, data) {
    var msg = data.message;

    var payload = msg.payload;

    $rootScope.dataModal = {};
    switch(parseInt(payload.accion))
    {
      case 1:
          pasefitPaga();
          $rootScope.dataModal = {Mtitle:msg.title, Mtext: msg.text};
          break;
      case 2:
          rModalC();
          $rootScope.dataModal = {Mtitle:msg.title, Mtext: msg.text, Mid:msg.idTimeTable};
          break;
      case 3:
          cModalCC();
          $rootScope.dataModal = {Mtitle:msg.title, Mtext: msg.text, Mid:msg.idTimeTable};
          break;
      case 5:
          noMembre();
          $rootScope.dataModal = {Mtitle:msg.title, Mtext: msg.text};
          break;
      case 6:
          noMembre();
          $rootScope.dataModal = {Mtitle:msg.title, Mtext: msg.text};
          break;
      case 7:
          venMembre();
          $rootScope.dataModal = {Mtitle:msg.title, Mtext: msg.text};
          break;
      case 8:
          $rootScope.safeApply(function(){
              var img = "http://api.pase.fit"+payload.url;
              $rootScope.imageUrl = img;
              $scope.imageUrl = img;
              session.dataClient($localStorage.dataUsuario.data)
              .then(
                function(r){
                  $localStorage.dataUsuario.dataCliente = JSON.parse(r);
                },
                function(r){
                  console.log(r);
                });
          });
      break;
      case 9:
          $rootScope.safeApply(function(){
            var img = "http://api.pase.fit"+payload.url;
            $rootScope.splash = img;
            $scope.splash = img;
            session.dataClient($localStorage.dataUsuario.data)
            .then(
              function(r){
                $localStorage.dataUsuario.dataCliente = JSON.parse(r);
              },
              function(r){
                console.log(r);
              });
          });

      break;
    }
  });

  $rootScope.abrirModal = function(){
      $rootScope.modal.show();
  };
  $rootScope.clsModal = function(){
    $rootScope.modal.hide();
  };
  function cModalCC(){
      $ionicModal.fromTemplateUrl('templates/modales/cancelarClase.html',{
        scope: $rootScope
      })
      .then(
            function(modal){
            $rootScope.modal = modal;
            $rootScope.modal.show();
        });
  }
  function pasefitPaga(){
      $ionicModal.fromTemplateUrl('templates/modales/pasefitPaga.html',{
        scope: $rootScope
      })
      .then(
            function(modal){
            $rootScope.modal = modal;
            $rootScope.modal.show();
        });
  }
  function rModalC(){
      $ionicModal.fromTemplateUrl('templates/modales/reserClase.html',{
        scope: $rootScope
      })
      .then(
            function(modal){
            $rootScope.modal = modal;
            $rootScope.modal.show();
        });
  }
  function noMembre(){
      $ionicModal.fromTemplateUrl('templates/modales/noMembre.html',{
        scope: $rootScope
      })
      .then(
            function(modal){
            $rootScope.modal = modal;
            $rootScope.modal.show();
        });
  }
  function venMembre(){
      $ionicModal.fromTemplateUrl('templates/modales/venMembre.html',{
        scope: $rootScope
      })
      .then(
            function(modal){
            $rootScope.modal = modal;
            $rootScope.modal.show();
        });    
  }
});