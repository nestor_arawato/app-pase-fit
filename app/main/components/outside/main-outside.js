'use strict';
angular.module('main').component('mainOutside',
  {templateUrl: 'main/components/outside/main-outside.html',
    controller: mainOutsideCtrl});

function mainOutsideCtrl ($log) {
  var $ctrl = this;
  $ctrl.a = 'a';
  $log.log('mainOutsideCtrl');
}
