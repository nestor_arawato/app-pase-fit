'use strict';
angular.module('main').component('lista',
        {templateUrl: 'main/components/client/lista.html',
          controller: listaCtrl});

function listaCtrl(
    $scope,
    ListasFactory,
    canoa,
    session,
    $localStorage,
    $sessionStorage,
    $ionicLoading,
    $ionicSideMenuDelegate,
    $state,
    modalPerfil,
    Restangular,
    $ionicModal,
    $cordovaGeolocation,
    $timeout,
    $rootScope,
    $ionicPopover,
    $ionicPlatform,
    $ionicScrollDelegate,
    $http
  ){

// DECLARA AL INICIAR EL DASHBOARD ACTIVO

var vm = this;

vm.onDashboard = true;
vm.onMap = false;
vm.cargaUno = false;
vm.mostrarRefe = false;
vm.tareaFavorito = false;
$scope.listaClases = [];
$scope.sinResultado= false;
$scope.lista = true;
$scope.paginaClases = 0;
$scope.ListaGeneral = true;
$scope.fechaDeBusqueda = '';

  vm.crearModalCancelar = function(){
      $ionicModal.fromTemplateUrl('templates/modales/modalCancelarClase.html', {
        scope: $scope
      }).then(function(modal){
          $scope.modalCancelar = modal;
          $scope.success = false;
          $scope.error = false;
      });
  } 

  vm.acomodarHora = function(horaServidor){
    var hora = horaServidor.split(':');
    if(hora[0]>12){
        hora[0]= hora[0]-12;
        return hora[0]+":"+hora[1]+"pm";
      }
    else{
        return hora[0]+":"+hora[1]+"am";
      }
  }

  function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  vm.clasesDia = function(){    
    if($scope.fechaDeBusqueda == '' && $scope.fechaDeBusqueda == undefined && $scope.fechaDeBusqueda == null){
      var fechaSelect = new Date($localStorage.fechaOriginal.split(' ')[0]).toISOString();
      $scope.fechaDeBusqueda = fechaSelect.split('-')[2].substr(0,2)+"-"+fechaSelect.split('-')[1]+"-"+fechaSelect.split('-')[0];
    }

    $ionicLoading.show();
    if(!vm.onDashboard){
      $scope.listaClases = [];
      var busqueda = {
          page: $scope.paginaClases+1,
          state: ($sessionStorage.parametros !== undefined && $sessionStorage.parametros.estado !== ''  && $sessionStorage.parametros.estado !== null ) ? $sessionStorage.parametros.estado : null,
          city: ($sessionStorage.parametros !== undefined && $sessionStorage.parametros.ciudad !== null && $sessionStorage.parametros.ciudad !== '') ? JSON.stringify($sessionStorage.parametros.ciudad) : null,
          startTime: ($sessionStorage.parametros !== undefined &&  $sessionStorage.parametros.horas.hora !== null &&  $sessionStorage.parametros.horas.hora !== '') ? $sessionStorage.parametros.horas.hora : null,
          finishTime: ($sessionStorage.parametros !== undefined && $sessionStorage.parametros.horas.hora2 !==null && $sessionStorage.parametros.horas.hora2 !== '') ? $sessionStorage.parametros.horas.hora2 : null,
          amenities: ($sessionStorage.parametros !== undefined && $sessionStorage.parametros.comodidad !== null && $sessionStorage.parametros.comodidad !== '') ? JSON.stringify($sessionStorage.parametros.comodidad) : null,
          activities: ($sessionStorage.parametros !== undefined && $sessionStorage.parametros.actividad !== null && $sessionStorage.parametros.actividad !== '') ? $sessionStorage.parametros.actividad : null,
          day: ($scope.fechaDeBusqueda !== undefined && $scope.fechaDeBusqueda !== null && $scope.fechaDeBusqueda !== '') ? $scope.fechaDeBusqueda : null
        };

        ListasFactory.solicitudClases().customGET('',busqueda)
        .then(
          function(r){
              $scope.bsqStudio =[];
              $scope.listaClases = $.parseJSON(r);
              $scope.sinResultado=false;
                       
              for(var i=0;i<$scope.listaClases.length;i++){
                $scope.listaClases[i].tareaFavorito = false;
                if($scope.listaClases[i].image!=null){
                  if($scope.listaClases[i].image.split(":")[0]!="http"){
                    $scope.listaClases[i].image = "http://api.pase.fit/uploads/images/schedules/"+$scope.listaClases[i].image;
                  }
                }
              }

              $scope.paginaClases = 1;
              $scope.ultimaCantidad = $.parseJSON(r).length;
              vm.cargaUno = true;
              if($scope.ultimaCantidad == 0){
                $scope.sinResultado=true;
              }
              else{
                $scope.sinResultado=false;
              }

              if($ionicSideMenuDelegate.isOpenLeft()){
                $ionicSideMenuDelegate.toggleLeft();
              }



              $ionicLoading.hide();
          }
          ,function(r){

              if($scope.paginaClases <=1){
                $scope.sinResultado = true;
              } 

              $ionicLoading.hide();  

              if($ionicSideMenuDelegate.isOpenLeft()){
                  $ionicSideMenuDelegate.toggleLeft();
              }

          })
      }
      else{
        //BUSCANDO TODAS LAS CLASES RESERVADAS
        if($scope.fechaDeBusqueda !== $localStorage.fecha){
          // RESERVACIONES DE LA SEMANA
          ListasFactory.obtenerReservaciones().customGET('',
          {
            date:$scope.fechaDeBusqueda
          }).then(
          function(r){
            $ionicLoading.hide();
            $scope.listaClases = $.parseJSON(r[0]);

            for(var i=0; i<$scope.listaClases.length;i++){
              $scope.listaClases[i].noPasadas= true;
            }
          },
          function(r){
            $ionicLoading.hide();
            
            console.log('no cosiguió más clases reservadas');
          });
        }
        else{
          // RESERVACIONES DEL DIA ACTUAL
          ListasFactory.obtenerReservacionesActual().get('').then(
            function(r){

            $scope.listaClases = $.parseJSON(r[0]);
            
            var noPasadas = $.parseJSON(r[1]);

            for(var i=0; i<noPasadas.length;i++){
              noPasadas[i].noPasadas= true;
            }

            $scope.listaClases = $scope.listaClases.concat(noPasadas);
            $ionicLoading.hide();

            },
            function(r){
              $ionicLoading.hide();
              // if($scope.paginaClases <=1){
              //   $scope.sinResultado = true;
              // } 
            });
        }
        //BUSACANDO SUGERENCIAS
        vm.buscarSugerencias();
      }
    }
  vm.estudiosDia = function(){
    $scope.bsqStudio = [];
    $scope.listaClases = [];
    $ionicLoading.show(); 
    var busqueda = {
          city: ($sessionStorage.parametrosEstudio != null ) ? JSON.stringify($sessionStorage.parametrosEstudio.ciudad) : null,
          page:($scope.paginaClases+1),
          day: $scope.fechaDeBusqueda,
          activities: ($sessionStorage.parametrosEstudio != null ) ? $sessionStorage.parametrosEstudio.actividad : null
    };
    ListasFactory.obtenerListaStudio2().customGET('',busqueda)
    .then(
      function(r){
          $scope.bsqStudio = JSON.parse(r);
          $scope.bsqStudio = JSON.parse($scope.bsqStudio[0]);

          for(var i=0; i < $scope.bsqStudio.length;i++){
            $scope.bsqStudio[i].categorias = "";
            for(var a=0; a < $scope.bsqStudio[i].studioClasses.length;a++){
              $scope.bsqStudio[i].categorias = $scope.bsqStudio[i].studioClasses[a].classCategory.name+" - "+$scope.bsqStudio[i].categorias;
            }
          }
          if($scope.bsqStudio.length == 0){
            console.log("esta vacio");
            $scope.sinResultado= true;
          }
          $scope.listaClases = [];
          $ionicLoading.hide();

          if($ionicSideMenuDelegate.isOpenLeft()){
            $ionicSideMenuDelegate.toggleLeft();
          }

        },
      function(r){
          console.log(r);
          $ionicLoading.hide();  

          if($ionicSideMenuDelegate.isOpenLeft()){
            $ionicSideMenuDelegate.toggleLeft();
          }

          if($scope.paginaClases <=1){
              $scope.sinResultado = true;
          }        
      });
  };
  vm.buscarSugerencias = function(){
    vm.clasesSugerencia = {};
    vm.mostrarSuge = false;
      //BUSQUEDA DE LAS PREFERENCIAS 
          ListasFactory.obtenerSugerencias().customGET('',{
             page: 1,
             day: $scope.fechaDeBusqueda
           })
          .then(function(r){
              vm.clasesSugerencia = JSON.parse(r);
              if(vm.clasesSugerencia.length > 0){
                  for(var i=0;i<vm.clasesSugerencia.length;i++){
                        vm.clasesSugerencia[i].tareaFavorito = false;
                        if(vm.clasesSugerencia[i].schedule.image !== null){
                          if( vm.clasesSugerencia[i].schedule.image.split(":")[0] !== "http"){
                             vm.clasesSugerencia[i].schedule.image = "http://api.pase.fit/uploads/images/schedules/"+vm.clasesSugerencia[i].schedule.image;
                          }
                        }
                  }
              }
              else{
                $scope.sinResultado = true;
              }
              //CONDICIONAL POR SI TERMINA LA PROMESA DE SUGERENCIA ESTANDO EN CLASES
              if(vm.onDashboard)
                  vm.mostrarSuge = true;
          },null);
  };
  
  vm.acomodarFecha = function(hora){
    var cadena;
    cadena = canoa.calcularFecha(hora);
    return cadena[0]+" "+cadena[2]+" de "+cadena[1];
  };
  
  vm.masClases  = function(){
    if($scope.ultimaCantidad === 10){
      $ionicLoading.show();
      ListasFactory.solicitudClases().customGET('',{page:($scope.paginaClases+1),day:$scope.fechaDeBusqueda}).then(
        function(r){
          $scope.paginaClases++;
          $scope.listaClases = $scope.listaClases.concat($.parseJSON(r));
          $ionicLoading.hide();
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.ultimaCantidad = $.parseJSON(r).length;
          $scope.sinResultado= false;   
        },
        function(r){
          if($scope.paginaClases <=1){
            $scope.sinResultado = true;
          } 
          $ionicLoading.hide();            
        }
      );
    }
  };
  $scope.$on('eventoBuscar',function(evt,arg){
    vm.onDashboard=false;
    vm.mostrarSuge = false;
    $scope.ListaGeneral = true;

    //GUARDANDO PARAMETROS DE BUSQUEDAS
    $sessionStorage.parametros = arg.busqueda;
    if($scope.paginaClases>0){
      $scope.paginaClases=0;
    }
     vm.clasesDia();
    // listas.solicitudClases()
    // .customGET('', 
    //   {
    //   page: $scope.paginaClases+1,
    //   city: (arg.busqueda.ciudad !== null && arg.busqueda.ciudad !== undefined && arg.busqueda.ciudad !== '') ? JSON.stringify(arg.busqueda.ciudad) : null,
    //   startTime: (arg.busqueda.horas.hora !== null && arg.busqueda.horas.hora !== undefined && arg.busqueda.horas.hora !== '') ? arg.busqueda.horas.hora : null,
    //   finishTime: (arg.busqueda.horas.hora2 !== null && arg.busqueda.horas.hora2 !== undefined && arg.busqueda.horas.hora2 !== '') ? arg.busqueda.horas.hora2 : null,
    //   amenities: (arg.busqueda.comodidad !== null && arg.busqueda.comodidad !== undefined && arg.busqueda.comodidad !== '') ? JSON.stringify(arg.busqueda.comodidad) : null,
    //   activities: ( arg.busqueda.actividad !== null && arg.busqueda.actividad !== undefined && arg.busqueda.actividad !== '') ? arg.busqueda.actividad : null,
    //   day: $scope.fechaDeBusqueda
    //   })
    // .then(
    //   function(r){
    //     // console.log(r);
    //     $scope.paginaClases++;
    //     $scope.bsqStudio = [];
    //     $scope.listaClases = $.parseJSON(r);
    //     for(var i=0;i<$scope.listaClases.length;i++){
    //       $scope.listaClases[i].tareaFavorito = false;
    //     }
    //     $ionicLoading.hide();
    //     $ionicSideMenuDelegate.toggleLeft();
    //     $scope.ultimaCantidad = $.parseJSON(r).length;
    //     if($scope.listaClases.length == 0){
    //       $scope.sinResultado = true;
    //     }
    //     // else{
    //       // console.log('tiene algo: '+$scope.listaClases.length);
    //       // $scope.sinResultado= false;
    //     // }

    //   },
    //   function(r){
    //     if($scope.paginaClases <=1){
    //         $scope.sinResultado = true;
    //     } 
    //     $ionicLoading.hide();

    //   })
  });

$scope.$on('eventoBuscarEstudio',function(evt,arg){
    $scope.sinResultado= false;
    $scope.ListaGeneral= false;
    $sessionStorage.parametrosEstudio = arg.busqueda;
    $ionicLoading.show();
    vm.estudiosDia();
    // listas.obtenerListaStudio2().customGET('',
    //   {
    //       activities:( arg.busqueda.actividad !== undefined && arg.busqueda.actividad !== null && arg.busqueda.actividad !== '') ? arg.busqueda.actividad : null ,
    //       city: (arg.busqueda.ciudad !== null && arg.busqueda.ciudad !== undefined && arg.busqueda.ciudad !== '') ? JSON.stringify(arg.busqueda.ciudad) : null,
    //       page:1,
    //       day:$scope.fechaDeBusqueda
    //   })
    // .then(
    //   function(r){
    //       $scope.bsqStudio = JSON.parse(r);
    //       $scope.bsqStudio = JSON.parse($scope.bsqStudio[0]);

    //       for(var i=0; i < $scope.bsqStudio.length;i++){
    //         $scope.bsqStudio[i].categorias = "";
    //         for(var a=0; a < $scope.bsqStudio[i].studioClasses.length;a++){
    //           $scope.bsqStudio[i].categorias = $scope.bsqStudio[i].studioClasses[a].classCategory.name+" - "+$scope.bsqStudio[i].categorias;
    //         }
    //       }
    //       if($scope.bsqStudio.length == 0){
    //         $scope.sinResultado= true;
    //       }
    //       $scope.listaClases = [];
    //       $ionicLoading.hide();
    //       $ionicSideMenuDelegate.toggleLeft();
    //   },
    //   function(r){
    //       if($scope.paginaClases <=1){
    //         $scope.sinResultado = true;
    //       } 
    //       $ionicLoading.hide();          
    //   });
});

vm.mostrarModal = function(){
  modalPerfil.open();
};
$scope.close = function () {
  closeAndRemove(modalInstance);
};

vm.Favorito = function(clase){
  if(!clase.tareaFavorito){
        clase.tareaFavorito = true;
      Restangular.configuration.getIdFromElem = function (elem) {
         var id = "";
         if (elem["@id"] === undefined) {
           id = elem.id;
         } else {
           id = elem["@id"].split("/")[3];
         }
         return id;
       };
      var idCliente = $localStorage.dataUsuario.data.profiles_iri[0].client_url;
      idCliente = idCliente.split('/');
      if( !(clase.schedule.favorite) ) {        
          session.cliente().get(idCliente[3]).then(
              function(r) {
                  var clienteActual = r;
                  clienteActual.schedules.push("/api/schedules/" + clase.schedule.id);
                  clienteActual.put()
                  .then(
                      function(r){
                          clase.schedule.favorite = true;
                          clase.tareaFavorito = false;
                      },
                      function(r){
                          clase.tareaFavorito = false;
                          clase.schedule.favorite = false;
                      })
              },
              function(r) {
                      clase.tareaFavorito = false;
                      clase.schedule.favorite = false;
              })
        }
        else{
              session.quitarFavorito().get('',{id: clase.schedule.id}).then(
                function(r){
                          clase.tareaFavorito = false;
                          clase.schedule.favorite = false;
                }
                ,function(r){
                          clase.tareaFavorito = false;
                          clase.schedule.favorite = true;
                });
        }
  }
}
  vm.crearModarl = function(){
      $ionicModal.fromTemplateUrl('templates/modales/mensajeReservar.html', {
        scope: $scope
      }).then(function(modal){
          $scope.modal = modal;
          // vm.mensajeData();
          $scope.success = false;
          $scope.error = false;
      });
  }

  vm.reservarClase = function(publico){
      $scope.post = {
          timeTable :"/api/time_tables/"+$scope.claseSelect.id,
          client:$localStorage.dataUsuario.data.profiles_iri[0].client_url,
          public: publico
      };
      $ionicLoading.show();
      ListasFactory.reservarClass().post($scope.post)
      .then(
        function(r){
            $ionicLoading.hide();
            $scope.success = true;
            $scope.$broadcast('quitarPlus');
            ($scope.claseSelect.idReserve == null) ? $scope.claseSelect.idReserve = r['@id'].split('/')[3] : $scope.claseSelect.idReserve ;
        },
        function(r){
            $ionicLoading.hide();
            $scope.error = true;
            switch(r.status){
                case 340: $scope.tituloError ="Tienes 3 Strikes, paga tu penalización";break;
                case 350: $scope.tituloError ="No tiene ninguna Membresía"; break;
                case 360: $scope.tituloError ="Para el dia seleccionado no tendrá membresía"; break;
                case 380: $scope.tituloError ="Su membresía se ha vencido"; break;
                case 390: $scope.tituloError = "Se han acabado los cupos de tu membresía"; break;
                case 400: $scope.tituloError ="La clase elegida no tiene cupos disponibles";break;
                case 415: $scope.tituloError ="Recuerda que puedes reservar hasta 45min antes de la clase";break;
                case 420: $scope.tituloError ="La clase elegida ya paso";break;
                case 430: $scope.tituloError ="Ya ha reservado una clase en este horario";break;
                case 450: $scope.tituloError ="Ya estas registrado para esta clase";break;
                case 510: $scope.tituloError ="Aun no puede reservar esta clase";break;
                case 550: $scope.tituloError ="Ya tiene 4 reservaciones en este estudio para el mes seleccionado";break;
                default: $scope.tituloError = "No se puede reservar una clase"; break;  
            }
      });
  }
  vm.llenarModal = function(){
      console.log('llenando modal');
      $scope.mensajeData = {};
      $scope.mensajeData.fecha = canoa.calcularFecha($scope.claseSelect.date);
      $scope.mensajeData.tituloClase = $scope.claseSelect.schedule.className;
      $scope.mensajeData.nombreStudio = $scope.claseSelect.schedule.studioClass.studio.studioName;
      $scope.mensajeData.direStudio = $scope.claseSelect.schedule.studioClass.studio.studioCityName;
      $scope.mensajeData.horaComienzo = vm.acomodarHora($scope.claseSelect.schedule.classStartTime);
      console.log($scope.mensajeData.horaComienzo);
      $scope.mensajeData.horaFin = vm.acomodarHora($scope.claseSelect.schedule.classFinishTime);
  }
  vm.cerrarModal = function(){
      $scope.modal.hide();
  }
  // MODAL RESERVAR
  vm.abrirModal = function(clase){ 
      $scope.claseSelect = clase;
      $scope.modal.show();
      vm.llenarModal();
      $scope.success = false;
      $scope.error = false; 

      $scope.$on('quitarPlus',function(){
        clase.reserved = true;
      })       
  }
  vm.close = function () {
      closeAndRemove(modalInstance);
  }
  // MODAL CANCELAR
  vm.abrirCancelar = function(clase) {
      vm.indexElim = $scope.listaClases.indexOf(clase);
      $scope.claseSelect = clase;
      $scope.modalCancelar.show();
      vm.llenarModal();
      $scope.success = false;
      $scope.error = false;  
  }
  vm.cerrarModalCancelar = function() {
      $scope.modalCancelar.hide();
  }
  vm.cancelarClase = function() {
    $ionicLoading.show();
    if($scope.claseSelect.idReserve!=null){
      $http({
              method: 'DELETE',
              url: 'http://api.pase.fit/api/reserves/' + $scope.claseSelect.idReserve
          })
      .then(
        function(d){
          $scope.claseSelect.reserved = null;
          $ionicLoading.hide();
          if(vm.onDashboard){
            console.log("pasa por removerItemClass");
            vm.removerItemClass(vm.indexElim);
          }

            switch(d.status)
            {
              case 210 : 
                  $scope.modalCancelar.hide();
                  $scope.claseSelect.idReserve=null;
                  $localStorage.membresia.strikes = $localStorage.membresia+1;
                  $scope.strikes = $localStorage.membresia.strikes;
                  $scope.modalStrike.show();
              break;
              case 200 : 
                  $scope.modalCancelar.hide();
                  $scope.claseSelect.idReserve=null;
              break;
              case 204:
                  $scope.modalCancelar.hide();
                  $scope.claseSelect.idReserve=null;
              break;
            }
            console.log($scope.claseSelect);
        },
        function(d){
          $scope.modalCancelar.hide()
          $ionicLoading.hide();
          $timeout(
            function(){
                $ionicLoading.show({template:"¡Imposible encontrar reserva!"});
            }
          ,3000);

        })
      }
  }    
  // MAPA DE GOOGLE 
  vm.crearMapa = function() {
    $rootScope.safeApply(function(){
        vm.onMap = true;
        $ionicScrollDelegate.scrollTop();
        vm.makeMap();
      });
  }
  //WATCH DE LAS BUSQUEDAS
  $scope.$watch('fechaDeBusqueda', function(newV,oldV) {
    if($scope.fechaDeBusqueda === '' || $scope.fechaDeBusqueda === undefined || $scope.fechaDeBusqueda === null){
      var fechaSelect = new Date($localStorage.fechaOriginal.split(' ')[0]).toISOString();
      $scope.fechaDeBusqueda = fechaSelect.split('-')[2].substr(0,2)+"-"+fechaSelect.split('-')[1]+"-"+fechaSelect.split('-')[0];
    }

    if(newV !== '')
      $scope.fechaDeBusqueda = newV;
    
    $scope.paginaClases = 0;
    if($scope.ListaGeneral){
        if($sessionStorage.parametros == null && $sessionStorage.parametros == undefined){
            $sessionStorage.parametros = {
              estado:null,
              ciudad:null,
              actividad:null,
              comodidad:null,
              horas:{hora:null,hora2:null}
            }
        }
        vm.clasesDia();
        // console.log('lista general');
    }
    else{
      vm.estudiosDia();
      if($sessionStorage.parametrosEstudio == null && $sessionStorage.parametrosEstudio == undefined){
          $sessionStorage.parametrosEstudio = {
              ciudad: null,
              actividad:null
          }
      } 
      // console.log('lista del estudio');
    }
  });

  //EVENTOS DEL MENU PERFIL

    $rootScope.$on('calendarioEvent',function(event, args) {
      vm.onDashboard = true;
      $scope.listaClases = [];
    });

    $rootScope.$on('clasesEvent',function(event,args){
      vm.onDashboard = false;
      vm.mostrarSuge = false;
      vm.clasesDia();
    })

  vm.buscarStudio = function(studioNombre){
    studioNombre = studioNombre.toLowerCase();
    if(studioNombre.length>0){
      ListasFactory.obtenerListaStudio().customGET('',
        {
          name:studioNombre,
          page:1
        })
      .then(
        function(r){
          vm.busquedaStudio = JSON.parse(r);
        },
        function(r){
            // if($scope.paginaClases <=1){
            //     $scope.sinResultado = true;
            // } 
        });
    }
    else{
      vm.busquedaStudio = {};
    }

  }

  vm.irPerfilStudio = function(id) {
    $state.go('studioPerfil.detalle',{idStudio:id});
  }

  vm.abrirPopoperStudio = function(i) {

    $ionicPopover.fromTemplateUrl('templates/popover/perfilStudio.html',{
      scope: $scope
    })
    .then(function(popover){
      $scope.popover = popover;

      if($scope.bsqStudio[i]!=null) {
        $scope.studioInfo = $scope.bsqStudio[i];
          if($scope.studioInfo.dateOpen!== null &&  $scope.studioInfo.dateOpen!== undefined) {
              var horarioOpen = new Date($scope.studioInfo.dateOpen.timestamp * 1000);
              horarioOpen = formatDate(horarioOpen);
              $scope.studioInfo.dateOpen.formateado =  horarioOpen;
          }
          else
               var horarioOpen  = "";

          if($scope.studioInfo.dateClose!= null &&  $scope.studioInfo.dateClose!= undefined ) {
              var horarioClose = new Date($scope.studioInfo.dateClose.timestamp * 1000);
              horarioClose = formatDate(horarioClose);
              $scope.studioInfo.dateClose.formateado =  horarioClose;
          }
          else
                var horarioClose = "";    
      }
      else {
          $scope.studioInfo = $scope.listaClases[i].schedule.studioClass.studio;

          if($scope.studioInfo.dateOpen!== null &&  $scope.studioInfo.dateOpen!== undefined) {
              var horarioOpen = new Date($scope.studioInfo.dateOpen.timestamp * 1000);
              horarioOpen = formatDate(horarioOpen);
              $scope.studioInfo.dateOpen.formateado =  horarioOpen;
          }
          else
               var horarioOpen  = "";

          if($scope.studioInfo.dateClose!= null &&  $scope.studioInfo.dateClose!= undefined ) {
              var horarioClose = new Date($scope.studioInfo.dateClose.timestamp * 1000);
              horarioClose = formatDate(horarioClose);
              $scope.studioInfo.dateClose.formateado =  horarioClose;
          }
          else
                var horarioClose = "";          
      }

      $scope.popover.show();
    })
  }

  vm.buscadorAct = function() {
    $scope.$emit('buscadorAct');
  }

  vm.crearModalStrike = function() {
    $ionicModal.fromTemplateUrl('templates/modales/modalStrike.html',{
      scope: $scope
    }).then(function(modal){
      $scope.modalStrike = modal;
    })
  }
  vm.cerrarModalStrike = function(){
    $scope.modalStrike.hide();
  }    
  // vm.clasesDia();
  vm.crearModarl();
  vm.crearModalCancelar();
  vm.crearModalStrike();

  vm.removerItemClass = function(index){
    $scope.listaClases.splice(index,1);
  }
  vm.makeMap = function(position){  
         if ($scope.map === null || $scope.map === undefined || $scope.map === ""){
            var latLng = new google.maps.LatLng(10.503709, -66.843264);    
            var mapOptions = {
               center: latLng,
               zoom: 15,
               streetViewControl: false,
               zoomControl:false,
               noClear: true
             };
             $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
         }
         else{
            for (var i = 0; i < $scope.marcadores.length; i++){
                $scope.marcadores[i].setMap(null);
            }  
         }
                $scope.marcadores = [];
                if($scope.bsqStudio.length == 0){
                  for(var d=0;d<$scope.listaClases.length;d++){
                    if($scope.listaClases[d].schedule.studioClass.studio.location !== null && $scope.listaClases[d].schedule.studioClass.studio.location !== undefined){
                      var location = $scope.listaClases[d].schedule.studioClass.studio.location.split(',');
                      location = {lat:parseFloat(location[0]),lng:parseFloat(location[1])};

                      var m = new google.maps.Marker({
                         map: $scope.map,
                         animation: google.maps.Animation.DROP,
                         position: location
                       });
                      m.set('id',d);
                      $scope.marcadores.push(m);
                    }
                  }
                }
                else{
                    for(var i = 0; i<$scope.bsqStudio.length; i++){ 
                      if($scope.bsqStudio[i].location !== undefined && $scope.bsqStudio[i].location !== null && $scope.bsqStudio[i].location !== ""){
                        var location = {lat:parseFloat($scope.bsqStudio[i].location),lng:parseFloat($scope.bsqStudio[i].lon)};
                        var m = new google.maps.Marker({
                          map: $scope.map,
                          animation: google.maps.Animation.DROP,
                          position: location,
                        });
                        m.set('id',i);

                        $scope.marcadores.push(m);     
                      }
                    } 
                }

                for (var j = 0; j <= $scope.marcadores.length-1; j++){
                    $scope.marcadores[j].addListener('click', function(){
                      vm.abrirPopoperStudio(this.id);
                    });
                }
  }
}


