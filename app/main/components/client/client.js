'use strict';
angular.module('main').component('client',
  {templateUrl: 'main/components/client/client.html',
    controller: clientCtrl});

function clientCtrl ($log) {
  var $ctrl = this;
  $ctrl.a = 'a';
  $log.log('clientCtrl');
}
