'use strict';
angular.module('main').component('register',
  {templateUrl: 'main/components/access/register.html',
    controller: registerCtrl});

function registerCtrl ($log, RegisterFactory,
    $state,
    $localStorage,
    $ionicLoading,
    $timeout,
    $ionicAuth,
    $ionicUser,
    $http,
    session,
//    listas,
    ListasFactory,
    $rootScope) {
  var $ctrl = this;

  $log.log('registerCtrl');
  
    $ctrl.selected = false;
    $ctrl.studio = false;
    $ctrl.client = false;    

    $ctrl.selectType = function(type){
        console.log(type);
        $ctrl.typeSelected = type;
        $ctrl.selected = true;
    };
    $ctrl.registrar = function(datosFormulario){
        //REGISTRA AL USUARIO DESDE EL SERVICIO
        datosFormulario.fos_user_registration_form.username = datosFormulario.fos_user_registration_form.email;
        if($ctrl.typeSelected === "studio"){
            datosFormulario.fos_user_registration_form.roles= "ROLE_STUDIO";
        }
        else if($ctrl.typeSelected ==="client"){
            datosFormulario.fos_user_registration_form.roles= "ROLE_CLIENT";
        };
        
        RegisterFactory.registrar(datosFormulario);
    };
    
    $ctrl.registrarFacebook  = function(){
        $ionicLoading.show();
        console.log('Registrando por facebook...');
        $ionicAuth.login('facebook')
        .then(function(r){

            var data  = {};
            console.log(r);
            console.log($ionicAuth);

            data.email = $ionicUser.social.facebook.data.email;
            data.first_name = $ionicUser.social.facebook.data.raw_data.first_name;
            data.gender = "male";
            data.last_name = $ionicUser.social.facebook.data.raw_data.last_name;

            var config = {method: 'POST',
//            url: "http://api.pase.fit/social/facebook",
            url: "http://api.pase.fit/social/facebook",
            skipAuthorization: true,
            data: data};

            $http(config).then(
                function(r){
                    console.log('exito');
                    $localStorage.token = r.data.token;
                    $localStorage.refreshtoken = r.refresh_token;
                    $ionicLoading.show({template:"Iniciando Sesión con Facebook.."});
                    $localStorage.dataUsuario =r.data.data;
                    $localStorage.fechaOriginal = r.data.data.date.date;

                     var fecha = r.data.data.date.date.split(' ')[0];
                    fecha = fecha.split('-');
                    $localStorage.fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];

                    console.log($localStorage.dataUsuario.date.date);
                    console.log($localStorage.dataUsuario.roles[0]);

                        switch($localStorage.dataUsuario.roles[0]){
                            case 'ROLE_CLIENT' :
                                $localStorage.login = true;
                                $ctrl.buscarCliente($localStorage.dataUsuario);
                            break;

                            // case 'ROLE_STUDIO' :
                            //     $ctrl.buscarStudio($localStorage.dataUsuario.data);
                            // break;
                        }

                },
                function(r){
                    console.log('fallido');
                    $ctrl.errorFacebook();                    
                    console.log(r);
                });

        },function(r){
            console.log('fallido');
            $ctrl.errorFacebook();
            console.log(r);
        });
    };

    $ctrl.buscarCliente = function(dataCliente){
        session.dataClient(dataCliente).then(
            function(r){

                ListasFactory.obtenerCuotas().get('')
                .then(
                  function(d){
                    console.log(d);
                    $rootScope.classReserva = (d[1] === null) ? 0 : d[1];
                    $localStorage.classReserva = (d[1] === null) ? 0 : d[1];
                  },
                  function(d){
                    console.log(d);
                  });

                $localStorage.dataUsuario.dataCliente = $.parseJSON(r);
                $ionicLoading.hide();
                $rootScope.firstName = $localStorage.dataUsuario.dataCliente.firstName;
                $rootScope.imageUrl = 'http://api.pase.fit//uploads/images/clients/'+$localStorage.dataUsuario.dataCliente.image;
                $state.go('menu.lista');

                ListasFactory.obtenerCiudades().customGET('',{id: $localStorage.dataUsuario.dataCliente.state, consultType: 'elastica'})
                .then(
                    function(b){
                        // console.log(b);
                        angular.forEach(JSON.parse(b), function(value, key) {
                              if(value.id === $localStorage.dataUsuario.dataCliente.city)
                              {
                                $localStorage.ciudad = value.city;
                                $rootScope.ciudad = $localStorage.ciudad;
                                console.log($rootScope.ciudad);
                              }
                            });
                    },
                    function(b){
                        console.log(b);
                    });
            },
            function(r){
                $ctrl.errorServidor();
            }
        );
    };

    $ctrl.errorFacebook = function(){
        $ionicLoading.hide();
        $ionicLoading.show({template:"Ocurrió un Error al registrar con Facebook"});
        $timeout(function(){
            $ionicLoading.hide();
        },3000);
    };    
}
