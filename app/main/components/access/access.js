'use strict';
angular.module('main').component('access',
  {templateUrl: 'main/components/access/access.html',
    controller: accessCtrl});

function accessCtrl ($log) {
  var $ctrl = this;
  $ctrl.a = 'a';
  $log.log('accessCtrl');
}
