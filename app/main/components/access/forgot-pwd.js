'use strict';
angular.module('main').component('forgotPwd',
  {templateUrl: 'main/components/access/forgot-pwd.html',
    controller: forgotPwdCtrl});

function forgotPwdCtrl ($log, $ionicLoading, RegisterFactory) {
  var $ctrl = this;
  $log.log('forgotPwdCtrl');
  
  $ctrl.recuperar = function(correo){
    $ctrl.usuario.username=null;
        $ionicLoading.show({
            template:"Enviando Solicitud."
        });

        RegisterFactory.recuperar(correo).then(function(r){
            $ctrl.resultado(r.message);   
                
        },function(r){
            $ctrl.resultado(r.message);
        });
    };

    $ctrl.resultado = function(msj){
           $ionicLoading.hide();

            $ionicLoading.show({
                template:msj
            });

            $timeout(function(){
                $ionicLoading.hide();
            },3000);
    };
}
