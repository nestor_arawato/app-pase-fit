'use strict';
angular.module('main').component('successRegister',
  {templateUrl: 'main/components/access/success-register.html',
    controller: successRegisterCtrl});

function successRegisterCtrl ($log) {
  var $ctrl = this;
  $ctrl.a = 'a';
  $log.log('successRegisterCtrl');
}
