'use strict';
angular.module('main').component('login',
        {templateUrl: 'main/components/access/login.html',
          controller: loginCtrl});

function loginCtrl($log,
        $scope,
        $state,
        session,
        $ionicHistory,
        $localStorage,
        $ionicLoading,
        $rootScope,
        $timeout,
        $ionicAuth,
        $ionicUser,
        $http,
        $ionicPush,
//        listas,
        ListasFactory,
        $sessionStorage
        ) {
  var $ctrl = this;
  $ctrl.a = 'a';
  $log.log('loginCtrl');


  $ctrl.selectRL = true;
  $ctrl.lgNormal = false;
  $ctrl.selectFN = false;

  $ctrl.iniSession = function (usuario) {
    $ionicLoading.show();

    //INICIA SESSION Y REDIRECCIONA A /Perfil
    session.iniciarSession(usuario).then(
            function (res) {
              $localStorage.token = res.token;
              var fecha = res.data.date.date.split(' ')[0];
              fecha = fecha.split('-');
              $localStorage.fecha = fecha[2] + '-' + fecha[1] + '-' + fecha[0];
              $localStorage.fechaOriginal = res.data.date.date;
              $localStorage.dataUsuario = res;
              $localStorage.membresia = res.membership;
              $ctrl.obtenerComodidades();
              $ctrl.buscarActividades();
              switch ($localStorage.dataUsuario.data.roles[0]) {
                case 'ROLE_CLIENT' :
                  console.log($localStorage.dataUsuario.data);
                  $localStorage.login = true;
                  $ctrl.buscarCliente($localStorage.dataUsuario.data);
                  break;

                case 'ROLE_STUDIO' :
                  $localStorage.login = true;
                  $ctrl.buscarStudio($localStorage.dataUsuario.data);
                  break;
              }
              $ctrl.ionicPushRegister();
            },
            function (r) {
              $ionicLoading.hide();
              console.log(r);
              switch (r.status) {
                case 401:
                  $ctrl.error401();
                  break;
                case 0:
                  $ctrl.error0();
                  break;
                default:
                  $ctrl.errorServidor();
              }
            });
    //QUITA EL REGRESAR LUEGO DE DESLOGUEAR
    $ionicHistory.nextViewOptions({disableBack: true});
  };
  $ctrl.buscarCliente = function (dataCliente) {
    console.log(dataCliente);
    session.dataClient(dataCliente).then(
            function (r) {
              ListasFactory.obtenerCuotas().get('')
                      .then(
                              function (d) {
                                $rootScope.classReserva = (d[1] === null) ? 0 : d[1];
                                $localStorage.classReserva = (d[1] === null) ? 0 : d[1];
                              },
                              function (d) {

                              });

              $localStorage.dataUsuario.dataCliente = $.parseJSON(r);
              $ionicLoading.hide();
              $rootScope.firstName = $localStorage.dataUsuario.dataCliente.firstName;
              $rootScope.imageUrl = ($localStorage.dataUsuario.dataCliente.image === null &&
                      $localStorage.dataUsuario.dataCliente.image === undefined) ? 'img/default-user-image.png' : 'http://api.pase.fit/uploads/images/clients/' + $localStorage.dataUsuario.dataCliente.image;
              $localStorage.imageUrl = $rootScope.imageUrl;

//              $state.go('menu.lista');
              $state.go('app.client.list');

              ListasFactory.obtenerCiudades().customGET('', {id: $localStorage.dataUsuario.dataCliente.state, consultType: 'elastica'})
                      .then(
                              function (b) {
                                angular.forEach(JSON.parse(b), function (value, key) {
                                  if (value.id === $localStorage.dataUsuario.dataCliente.city) {
                                    $localStorage.ciudad = value.city;
                                    $rootScope.ciudad = $localStorage.ciudad;
                                  }
                                });
                              },
                              function (b) {
                                console.log(b);
                              });
            },
            function (r) {
              $ctrl.errorServidor();
            }
    );
  };

  $ctrl.buscarStudio = function (dataCliente) {
    session.dataStudio(dataCliente).then(
            function (r) {
              $localStorage.dataUsuario.dataStudio = $.parseJSON(r);
              $rootScope.firstName = $localStorage.dataUsuario.dataStudio.studioName;
              $rootScope.imageUrl = ($localStorage.dataUsuario.dataStudio.logo === undefined &&
                      $localStorage.dataUsuario.dataUsuario.logo === null) ? 'img/default-user-image.png' : "http://api.pase.fit/uploads/images/logos/" + $localStorage.dataUsuario.dataStudio.logo;
              $rootScope.splash = ($localStorage.dataUsuario.dataStudio.image === undefined &&
                      $localStorage.dataUsuario.dataStudio === null) ? 'img/converUser.png' : "http://api.pase.fit/uploads/images/studios/" + $localStorage.dataUsuario.dataStudio.image;
              $rootScope.direccion = $localStorage.dataUsuario.dataStudio.studioCityName + " " + $localStorage.dataUsuario.dataStudio.studioStateName;
              $localStorage.imageUrl = $rootScope.imageUrl;
              $localStorage.splash = $rootScope.splash;
              $ionicLoading.hide();
              $state.go('listaStudio');
            }, function (r) {
      $ctrl.errorServidor();
    });
  };
  $scope.cambiarPass = function () {
    $state.go('recuPass');
  };

  $ionicHistory.clearHistory();

  // MENSAJES DE ERRORES DE LOGUEO
  $ctrl.errorServidor = function () {
    $ionicLoading.hide();
    $ionicLoading.show({
      template: 'No se pudó conectar con el servidor.'
    });
    $timeout(function () {
      $ionicLoading.hide();
    }, 3000);
  };
  $ctrl.noEstudioIn = function () {
    $ionicLoading.hide();
    $ionicLoading.show({template: "No puede iniciar como Un Estudio"});
    $timeout(function () {
      $ionicLoading.hide();
    }, 3000);
  };
  $ctrl.error401 = function () {
    $ionicLoading.hide();
    $ionicLoading.show({
      template: 'Credenciales incorrectos.'
    });
    $timeout(function () {
      $ionicLoading.hide();
    }, 3000);
  };
  $ctrl.error0 = function () {
    $ionicLoading.hide();
    $ionicLoading.show({
      template: 'Sin conexion a Internet.'
    });
    $timeout(function () {
      $ionicLoading.hide();
    }, 3000);
  };
  $ctrl.errorFacebook = function () {
    $ionicLoading.hide();
    $ionicLoading.show({template: "Ocurrió un Error al iniciar con Facebook"});
    $timeout(function () {
      $ionicLoading.hide();
    }, 3000);
  };

  $ctrl.iniciarFacebook = function () {
    $ionicLoading.show();
    console.log('iniciando por facebook...');
    $ionicAuth.login('facebook')
            .then(function (r) {

              var data = {};

              data.email = $ionicUser.social.facebook.data.email;
              data.first_name = $ionicUser.social.facebook.data.raw_data.first_name;
              data.gender = "male";
              data.last_name = $ionicUser.social.facebook.data.raw_data.last_name;

              var config = {method: 'POST',
                url: "http://api.pase.fit/social/facebook",
                skipAuthorization: true,
                data: data};

              $http(config).then(
                      function (r) {
                        console.log('exito');
                        $localStorage.token = r.data.token;
                        $localStorage.refreshtoken = r.refresh_token;
                        $ionicLoading.show({template: "Iniciando Sesión con Facebook.."});
                        $localStorage.dataUsuario = r.data.data;
                        var fecha = r.data.data.date.date.split(' ')[0];
                        fecha = fecha.split('-');
                        $localStorage.fecha = fecha[2] + '-' + fecha[1] + '-' + fecha[0];

                        $localStorage.fechaOriginal = r.data.data.date.date;

                        switch ($localStorage.dataUsuario.roles[0]) {
                          case 'ROLE_CLIENT' :
                            $localStorage.login = true;
                            $ctrl.buscarCliente($localStorage.dataUsuario);
                            break;

                            // case 'ROLE_STUDIO' :
                            //     $ctrl.buscarStudio($localStorage.dataUsuario);
                            // break;
                        }

                        $ctrl.ionicPushRegister();

                      },
                      function (r) {
                        $ctrl.errorFacebook();
                        console.log('fallido');
                        console.log(r);
                      });

            }, function (r) {
              console.log('fallido');
              $ctrl.errorFacebook();
              console.log(r);
            });


    // $ionicFacebookAuth.login().then(function(r){
    //     console.log('sin errores');
    //     console.log(r);
    // },
    // function(r){
    //     console.log('Error');
    //     console.log(r);
    // }
    // );
  };

  $ctrl.ionicPushRegister = function () {
    ionic.Platform
            .ready(
                    function () {
                      console.log('realizando push');
                      $ionicPush.register()
                              .then(function (t) {
                                console.log(t);
                                session.enviarToken().customPOST("", "", {token: t.token});
                                return $ionicPush.saveToken(t);
                              },
                                      function (r) {
                                        console.log(r);
                                      });
                    });
  };

  $ctrl.obtenerComodidades = function () {

    if ($sessionStorage.comodidades === null &&
            $sessionStorage.comodidades === undefined) {
      $ionicLoading.show();
      ListasFactory.obtenerComodidades().get('').then(
              function (r) {
                $sessionStorage.comodidades = r['hydra:member'];
                $ionicLoading.hide();
              },
              function (r) {
                console.log(r);
              });
    }
  };

  $ctrl.buscarActividades = function () {
    if ($sessionStorage.actividades === null && $sessionStorage.actividades === undefined) {
      $ionicLoading.show();
      ListasFactory.obtenerActividades().customGET().then(
              function (r) {
                $sessionStorage.actividades = r['hydra:member'];
                $ionicLoading.hide();
              },
              function (r) {
                console.log(r);
              });
    }
  };

}
