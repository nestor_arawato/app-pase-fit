angular.module('main')
.factory("Reserve", ["Restangular",
          function clientFactory(Restangular) {
            return Restangular.all("reserves");
            }])
.factory("ReservesAdmin", function (Reserve, Restangular) {
  var p = {
    updateReserve: function (reserve) {
      var r = Restangular.restangularizeElement(Reserve, reserve, Reserve);
      delete reserve.client;
      return r.put();
    },
    assist: function (reserve) {
      var status = 0;
      if (reserve.assist === 0) {
        status = 1;
      } else if (reserve.assist === 2) {
//        status = 3;
        status = 1;
      } else if (reserve.assist === 3) {
//        status = 3;
        status = 1;
      }
      reserve.assist = status;

      return this.updateReserve(reserve);
    },
    notAssist: function (reserve) {
      var status = 2;
      if (reserve.assist === 0) {
        status = 2;
      }else if(reserve.assist === 1){
        status = 3;
      }
      reserve.assist = status;
      return this.updateReserve(reserve);
    },

    correctAssist: function (reserve) {
      var status = 3;

      reserve.assist = status;
      return this.updateReserve(reserve);
    }
  };
  return p;
});