angular.module('main')
//.factory('registrar',registrarFact);
        .factory('RegisterFactory', ['$http', '$state', 'baseUrl', '$ionicLoading', '$timeout', registrarFact]);

//registrarFact.$inject = ['$http','$state','baseUrl','$ionicLoading','$timeout'];
//FUNCIONES DE FACTORY REGISTRAR

function registrarFact(
        $http,
        $state,
        baseUrl,
        $ionicLoading,
        $timeout) {
//  var registrar ={};
  var registrar = function (dataUsuario) {
    $ionicLoading.show();
    var configuracion = {
      method: 'POST', url: "http://api.pase.fit/app_dev.php/register/",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(dataUsuario)
    };
    $http(configuracion).then(
            function (r) {

              $ionicLoading.show({
                template: 'Registrado con éxito, revise su correo'
              });
              $timeout(function () {
                $ionicLoading.hide();
                $state.go('login');
              }, 3000);

            },
            function (r) {

              switch (r.status) {
                case 400:
                  $ionicLoading.show({
                    template: 'Correo ya en uso!. Utilice otro, Por favor.'
                  });
                  $timeout(function () {
                    $ionicLoading.hide();
                  }, 3000);
                  break;
                default:
                  $ionicLoading.show({
                    template: 'Error al conectarse con el servidor, intente más tarde.'
                  });
                  $timeout(function () {
                    $ionicLoading.hide();
                  }, 3000);
                  break;
              }

            });
  };
  var recuperar = function (usuario) {
    var resp = {
      'exito': false,
      'notfound': false,
      'fallido': true,
      'mensaje': null
    };
    var reset = baseUrl.all("/resetting/reset-password");

    return reset.customPOST({username: usuario.username});

  };
  var r = {
    registrar: registrar,
    recuperar: recuperar
  };
  return r;
}