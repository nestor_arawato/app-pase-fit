(function(){

	angular.module('main')
	.service('mensajes', function($ionicModal) {

    this.mensajeReservar = function() {

        var service = this;

        $ionicModal.fromTemplateUrl('templates/modales/mensajeReservar.html', {
          scope: null
          // controller: 'modalMensajeCtrl as ctrlMensaje'
        }).then(function(modal) {
            service.modal = modal;
            service.modal.show();
        });
    };

    this.ocultarMensaje = function() {
        this.modal.hide();
    };

});

})()