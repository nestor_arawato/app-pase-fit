angular.module('main')

.factory('baseUrl', function(Restangular) {
  return Restangular.withConfig(function(RestangularConfigurer) {
    RestangularConfigurer.setBaseUrl('http://api.pase.fit');
  });
})
.factory('basePase',function(Restangular) {
  return Restangular.withConfig(function(RestangularConfigurer) {
	RestangularConfigurer.setBaseUrl("http://api.pase.fit/");
 });
})
.factory('clientes',['Restangular',function(Restangular){
    return Restangular.all('clients');
}])
.factory('studios',['Restangular',function(Restangular){
    return Restangular.all('studios');
}])
.factory('studioClass',['Restangular',function(Restangular){
    return Restangular.all('studio_classes');
}])