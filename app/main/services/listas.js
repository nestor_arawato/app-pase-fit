angular.module('main')
        .factory('ListasFactory', ['studios', '$localStorage', '$state', 'Restangular', listasFact]);

function listasFact(studios, $localStorage, $state, Restangular) {
  var listas = {
    obtenerEstado: function () {
      return Restangular.all("findstates").customGET('', {consultType: 'elastica'});
    },
    obtenerCiudades: function () {
      return Restangular.all("findcities");
    },
    obtenerActividades: function () {
      return Restangular.all("class_categories");
    },
    obtenerComodidades: function () {
      return Restangular.all("amenities");
    },
    solicitudClases: function () {
      return Restangular.all("findclasses");
    },
    reservasDia: function () {
      return Restangular.all("findclient/classesday");
    },
    detalleClase: function () {
      return Restangular.all("findschedule");
    },
    detalleStudio: function () {
      return Restangular.all("findstudio");
    },
    sinParametros: function () {
      return studios.customGET("").then(function (r) {
        $localStorage.ListaBusqueda = r['hydra:member'];
      }, function (r) {
        console.log(r);
      });
    },
    obtenerLista: function () {
      return $localStorage.ListaBusqueda;
    },
    obtenerDetalle: function (posicion) {
      $state.go('tab.dtllStudio');
      return $localStorage.ListaBusqueda[posicion];
    },
    listaStudiosLlena: function () {
      if ($localStorage.ListaBusqueda !== null) {
        return true;
      } else {
        return false;
      }
    },
    obtenerDisciplinas: function () {
      return Restangular.all('classcategories');
    },
    reservarClass: function () {
      return Restangular.all('reserves');
    },
    obtenerProximas: function () {
      return Restangular.all('findclient/reservations');
    },
    obtenerTerminadas: function () {
      return Restangular.all('findclient/pastclasses');
    },
    obtenerFavoritas: function () {
      return Restangular.all('findclient/favoriteclasses');
    },
    obtenerReservacionesActual: function () {
      return Restangular.all("findclient/todayclasses");
    },
    obtenerReservaciones: function () {
      return Restangular.all("findclient/classesday");
    },
    obtenerPreferencias: function () {
      return Restangular.all("findclient/preferences");
    },
    obtenerClasesDiaPasadas: function () {
      return Restangular.all("findstudio/classesday/past");
    },
    obtenerClasesDiaFuturas: function () {
      return Restangular.all("findstudio/classesday/future");
    },
    obtenerClasesDiaStudio: function () {
      return Restangular.all("findstudio/classes");
    },
    obtenerListaReservaciones: function () {
      return Restangular.all("findschedule/reserves");
    },
    obtenerListaInsctructores: function () {
      return Restangular.all("findstudio/instructors");
    },
    obtenerListaStudio: function () {
      return Restangular.all("findstudios/name");
    },
    obtenerListaStudio2: function () {
      return Restangular.all("findstudios");
    },
    obtenerClfStudio: function () {
      return Restangular.all("findstudio/reputation");
    },
    obtenerClfClass: function () {
      return Restangular.all("findschedule/reputation");
    },
    eliminarHorarioClases: function () {
      return Restangular.all("time_tables");
    },
    editarClass: function () {
      return Restangular.all("schedules");
    },
    editarCliente: function () {
      return Restangular.all("clients");
    },
    calificarClass: function () {
      return Restangular.all("reputations");
    },
    editarStudio: function () {
      return Restangular.all("studios");
    },
    ObtenerListaPreferencia: function () {
      return Restangular.all("preferences");
    },
    editarPreferencia: function () {
      return Restangular.all("putpreferences");
    },
    obtenerCuotas: function () {
      return Restangular.all("findclient/quotes");
    },
    obtenerMembresia: function () {
      return Restangular.all("findclient/membership");
    },
    obtenerSugerencias: function () {
      return Restangular.all('findsuggestions');
    },
    obtenerCategoriaStudio: function () {
      return Restangular.all('studio_classes');
    }
  };
  return listas;
}