(function(){


	angular.module('main')
	.factory('canoa',["$state",canoa]);

		function canoa(){
		var canoa = {
				 data : {},
				 vaciarData: function(){
					data = {};
				},
				 obtenerData: function(){
					return data;
				},
				 asignarData: function(dataIn){
					data = dataIn;
				},
			   	calcularFecha: function(fecha){

			        fecha=fecha.split('-');

			        if(fecha.length!=3){
			                return null;
			        }

			        //Vector para calcular día de la semana de un año regular.
			        var regular =[0,3,3,6,1,4,6,2,5,0,3,5]; 
			        //Vector para calcular día de la semana de un año bisiesto.
			        var bisiesto=[0,3,4,0,2,5,0,3,6,1,4,6]; 
			        
			        //Vector para hacer la traducción de resultado en día de la semana.
			        var semana=['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

			        //Vector meses
			        var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];


			        var dia=fecha[0];
			        var mes=fecha[1]-1;
			        var anno=fecha[2];

			        if((anno % 4 == 0) && !(anno % 100 == 0 && anno % 400 != 0))
			            mes=bisiesto[mes];
			        else
			            mes=regular[mes];

			        var fechaLista = [semana[Math.ceil(Math.ceil(Math.ceil((anno-1)%7)+Math.ceil((Math.floor((anno-1)/4)-Math.floor((3*(Math.floor((anno-1)/100)+1))/4))%7)+mes+dia%7)%7)],meses[mes],dia];
			        
			        return fechaLista;
			    }				
		}
			return canoa;
		}

})()