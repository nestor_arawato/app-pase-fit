angular.module('main')
        .factory('modalPerfil', MyModal);

MyModal.$inject = ['$ionicModal', '$rootScope', '$localStorage', '$state', "$cordovaImagePicker", "$ionicLoading", "$timeout"];

function MyModal($ionicModal, $rootScope, $localStorage, $state, $cordovaImagePicker, $ionicLoading, $timeout) {
    var $scope = $rootScope.$new(),
            myModalInstanceOptions = {
                scope: $scope,
                animation: 'slide-in-left'
            };

    var myModal = {
        open: open,
        openStudio: openStudio
    };

    return myModal;

    function open() {
        $ionicModal.fromTemplateUrl('templates/modales/modalPerfil.html', myModalInstanceOptions)
                .then(
                        function (modalInstance)
                        {
                            $scope.cargandoFoto = false;
                            $scope.close = function () {
                                closeAndRemove(modalInstance);
                            };
                            $scope.changePhoto = function () {
//                                $scope.close();
                                var options = {
                                    maximumImagesCount: 1,
                                    width: 800,
                                    height: 800,
                                    quality: 100
                                };
//                                $timeout(function () {
                                    $cordovaImagePicker.getPictures(options)
                                            .then(function (results) {
                                                for (var i = 0; i < results.length; i++) {
                                                    console.log('Image URI: ' + results[i]);
                                                    $scope.pictureToSend = results[i];
                                                    $scope.uploadFile();
                                                }
                                            }, function (error) {
                                                alert(error);
                                            });
//                                }, 2000);
                            };
                            var viewUploadedPictures = function () {
                                $scope.imageUrl = $scope.pictureToSend;
                                $rootScope.imageUrl = $scope.pictureToSend;
                                $scope.cargandoFoto = false;
//                                $scope.$emit('changePhoto', $scope.pictureToSend);
//                                localStorage.setItem("preseeyl-image", JSON.stringify($scope.pictureToSend));
                                $ionicLoading.hide();
                            };
                            $scope.uploadFile = function () {
                                if ($scope.pictureToSend !== "" && $scope.pictureToSend !== null && $scope.pictureToSend !== undefined) {
                                    $ionicLoading.show({template: 'Cargando...'});
                                    $scope.cargandoFoto = true;
                                    var fileURL = $scope.pictureToSend;
                                    var options = new FileUploadOptions();
                                    options.fileKey = "file";
                                    options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
                                    options.mimeType = "image/jpeg";
                                    options.chunkedMode = true;
                                    options.headers = {
                                        'Authorization': "Bearer " + localStorage.getItem("ngStorage-token")
                                    };

                                    var params = {};
                                    params.file = $scope.pictureToSend;

                                    options.params = params;

                                    var ft = new FileTransfer();
                                    ft.upload(fileURL, encodeURI("http://api.pase.fit/api/clientImage"),
                                            viewUploadedPictures,
                                            function (error) {
                                                $scope.cargandoFoto = false;
                                                $ionicLoading.show({template: 'Error de Conexión...'});
                                                $ionicLoading.hide();
                                            }, options);
                                }
                            };
                            $scope.calendario = function () {
                                if (!$state.includes('menu.lista')) {
                                    $state.go('menu.lista');
                                }
                                $scope.$emit('calendarioEvent');
                            };
                            $scope.misClases = function () {
                                if (!$state.includes('menu.lista')) {
                                    $state.go('menu.lista');
                                }
                                $scope.$emit('clasesEvent');
                            };
                            return modalInstance.show();
                        });
    }

    function openStudio() {
        $ionicModal.fromTemplateUrl('templates/modales/modalPerfilStudio.html', myModalInstanceOptions)
                .then(
                        function (modalInstance)
                        {
                            $scope.close = function () {
                                closeAndRemove(modalInstance);
                            };
                            $scope.calendario = function () {
                                $scope.$emit('calendarioEvent');
                            };
                            $scope.misClases = function () {
                                $scope.$emit('clasesEvent');
                            };
                            return modalInstance.show();
                        });
    }


    function closeAndRemove(modalInstance) {
        return modalInstance.hide().then(function () {
            return modalInstance.remove();
        });
    }

}