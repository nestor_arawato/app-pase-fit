//angular.module('app.services')
angular.module('main')

.factory('session',sessionFact);
function sessionFact(
	$http,
	$localStorage,
	Restangular,
	$ionicModal,
	$ionicPush,
	baseUrl
	){
	
	var session = {
	iniciarSession: function(usuario){
		return Restangular.all("login_check").customPOST(usuario);
	},
	obtenerDatosPerfil : function(){
		return $localStorage.dataUsuario;
	},
	cerrarSession : function(){
		 // basePase.all("logout").post();
		 $localStorage.$reset();
		 $localStorage.login = false;
		 
	},
	dataClient: function(dataUsuario){
		console.log('mensaje de dataClient');
		console.log(dataUsuario);
		var clientIri = dataUsuario.profiles_iri[0].client_url.split('/');
    	return Restangular.all('findclient').customGET('',{'id':clientIri[3]});
	},
	dataStudio: function(dataUsuario){
		console.log('mensaje de dataStudio');
		var studioIri = dataUsuario.profiles_iri[0].studio_url.split('/');
            return Restangular.all("findstudio").customGET('',{'id':studioIri[3]})
	},
	obtenerHoraServidor: function(){
		return Restangular.all('currentdate');
	},
	mostrarMenu: function()	{
        $ionicModal.fromTemplateUrl('templates/modales/modalPerfil.html', {
          scope: null
          // controller: 'modalMensajeCtrl as ctrlMensaje'
        }).then(function(modal){
            // $scope.modal = modal;
            // vm.mensajeData();
            modal.show();
        });
	},
	cerrarModal : function(){
		modal.hide();
	},
	cliente:function(){
		return Restangular.all("clients");
	},
	quitarFavorito: function(){
		return Restangular.all("findclient/removefavorite");
	},
	enviarToken: function(){
		return Restangular.all("registerToken");
	},
	refreshToken: function(){
		console.log(baseUrl);
		return baseUrl.all('/token/refresh');
	}

	};
	return session;
}
