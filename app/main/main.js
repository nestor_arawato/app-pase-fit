'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'ionic-material',
  'ionMdInput',
  'ngStorage',
  'restangular',
  'angular-jwt',
  'ionic-datepicker',
  'ionic-timepicker',
  'ionic-multiselect',
  'angularMoment',
  'ionic.cloud'
]);

