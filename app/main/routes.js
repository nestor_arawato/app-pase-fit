angular.module('main')
        .config(function ($stateProvider, $urlRouterProvider) {

  // ROUTING with ui.router
  $urlRouterProvider.otherwise('/carrusel');
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('app', {
      url: '/',
      abstract: true,
      templateUrl: 'main/templates/menu.html',
      controller: 'MenuCtrl as menu'
    })
      .state('app.access', {
//        url: 'access',
        abstract: true,
        views: {
          'pageContent': {
            template: '<access></access>'
          }
        }
      })
      .state('app.access.login', {
        url: 'login',
        views: {
          'accessContent': {
            template: '<login></login>'
          }
        }
      })
      .state('app.access.register', {
        url: 'register',
        views: {
          'accessContent': {
            template: '<register></register>'
          }
        }
      })
      .state('app.access.forgot-pwd', {
        url: 'forgot-pwd',
        views: {
          'accessContent': {
            template: '<forgot-pwd></forgot-pwd>'
          }
        }
      })
      .state('app.access.reset-pwd', {
        url: 'reset-pwd',
        views: {
          'accessContent': {
            template: '<reset-pwd></reset-pwd>'
          }
        }
      })
      .state('app.access.success-register', {
        url: 'success-register',
        views: {
          'accessContent': {
            template: '<success-register></success-register>'
          }
        }
      })
      .state('app.outside', {
//        url: 'outside',
        abstract: true,
        views: {
          'pageContent': {
            template: '<main-outside></main-outside>'
          }
        }
      })
      .state('app.outside.carrusel', {
        url: 'carrusel',
        views: {
          'outsideContent': {
            template: '<carrusel></carrusel>'
          }
        }
      })
      .state('app.client', {
//        url: 'client',
        abstract: true,
        views: {
          'pageContent': {
            template: '<client></client>'
          }
        }
      })
      .state('app.client.list', {
        url: 'list',
        views: {
          'clientContent': {
//            templateUrl: 'main/templates/list.html'
            template: '<lista></lista>'
          }
        }
      })
      .state('app.listDetail', {
        url: '/list/detail',
        views: {
          'pageContent': {
            templateUrl: 'main/templates/list-detail.html'
            // controller: '<someCtrl> as ctrl'
          }
        }
      })
      .state('app.debug', {
        url: '/debug',
        views: {
          'pageContent': {
            templateUrl: 'main/templates/debug.html',
            controller: 'DebugCtrl as ctrl'
          }
        }
      });
});
